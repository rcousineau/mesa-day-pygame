import pygame

class PadSprite(pygame.sprite.Sprite):
    def __init__(self, number, position):
        pygame.sprite.Sprite.__init__(self)
        self.normal = pygame.image.load('blue-%d.png' % number)
        self.hit = pygame.image.load('green-%d.png' % number)
        self.number = number
        self.rect = pygame.Rect(self.normal.get_rect())
        self.rect.center = position
        self.image = self.normal
